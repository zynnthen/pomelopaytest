export function displayDateTime(dateTimeInString) {
    if (!isDate(dateTimeInString)) return "-";
    return displayDate(dateTimeInString) + " " + displayTime(dateTimeInString);
}

export function displayDate(dateTimeInString) {
    if (!isDate(dateTimeInString)) return "-";
    const dateObject = new Date(dateTimeInString);
    const year = dateObject.getFullYear();
    var month = dateObject.getMonth();
    var date = dateObject.getDate();

    if (date < 10) {
        date = "0" + date;
    }

    return `${date} ${checkMonth(month)} ${year}`;
}

export function displayTime(dateTimeInString) {
    if (!isDate(dateTimeInString)) return "-";
    const dateObject = new Date(dateTimeInString);
    var hours = dateObject.getHours();
    var minutes = "" + dateObject.getMinutes();

    if (hours < 10) {
        hours = "0" + hours;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }

    return `${hours}:${minutes}`;
}

function isDate(value) {
    return isNaN(value) && !isNaN(Date.parse(value)) ? true : false;
}

function checkMonth(monthIndex) {
    const months = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sept",
        "Oct",
        "Nov",
        "Dec"
    ];
    return months[monthIndex];
}