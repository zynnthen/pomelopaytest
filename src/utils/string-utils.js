export function firstCharUppercase(text) {
    if (text === undefined) return text;
    return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase()
}