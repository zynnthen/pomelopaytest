import _ from 'lodash';
import axios from 'axios';

import Constants from "../utils/constants";
import { TRANSACTION_LIST, TRANSACTION_UPDATE } from './types';

export const getTransactions = (path, callback) => async (dispatch) => {
    try {
        let { data } = await axios.get(`${Constants.BASE_URL}${path}`);
        if (data.hasOwnProperty("items") && data.hasOwnProperty("count")) {
            const array = _.values(data.items)
            dispatch({ type: TRANSACTION_LIST, payload: array, totalCount: data.count });
            callback(data, null);
        } else {
            callback(null, "Error: No transactions found.");
        }
    } catch (error) {
        console.warn(error);
        callback(null, error.message);
    }
}

export const updateTransaction = (transaction) => (dispatch) => {
    dispatch({ type: TRANSACTION_UPDATE, payload: transaction });
}