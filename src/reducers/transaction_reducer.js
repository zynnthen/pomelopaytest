import _ from 'lodash';
import {
    TRANSACTION_LIST, TRANSACTION_UPDATE
} from '../actions/types';

const INITIAL_STATE = {
    transactionList: [],
    transactionCount: 0
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case TRANSACTION_LIST:
            return {
                ...state,
                transactionList: action.payload,
                transactionCount: action.totalCount
            }
        case TRANSACTION_UPDATE:
            return {
                ...state,
                transactionList: state.transactionList.map(transaction => {
                    if (transaction.id === action.payload.id) {
                        return action.payload
                    }
                    return transaction
                })
            }
        default:
            return state;
    }
};
