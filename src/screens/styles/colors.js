export default {
    TEXT_HEADER: '#4B4B4B',
    TEXT_ERROR: '#FF0000',
    TEXT_PRIMARY: '#212121',
    TEXT_SECONDARY: '#727272',
    DISABLED: '#9e9e9e',

    DANGER: '#d0021b'
}
