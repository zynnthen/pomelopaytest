import { StyleSheet } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import COLORS from "./colors"

export default StyleSheet.create({
    container: {
        flex: 1
    },
    centerContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    flexBetweenContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    listItemContainer: {
        paddingTop: verticalScale(16),
        paddingLeft: scale(8),
        paddingRight: scale(8),
        paddingBottom: verticalScale(16)
    },
    headerText: {
        fontSize: moderateScale(24),
        color: COLORS.TEXT_HEADER
    },
    subtitle: {
        fontSize: moderateScale(20),
        color: COLORS.TEXT_SECONDARY
    },
    textMargin: {
        margin: scale(8)
    },
    listPrimaryTextStyle: {
        fontSize: moderateScale(20),
        color: COLORS.TEXT_PRIMARY
    },
    listSecondaryTextStyle: {
        fontSize: moderateScale(14),
        color: COLORS.TEXT_SECONDARY
    },
    errorText: {
        color: COLORS.TEXT_ERROR
    }
});