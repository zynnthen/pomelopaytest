import React, { Component } from 'react';
import { View, FlatList, Text } from 'react-native';
import { connect } from 'react-redux';

import Constants from "../../utils/constants";
import TransactionItem from '../components/TransactionItem';
import styles from '../styles/styles';

import { getTransactions } from '../../actions';

class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            transactions: [],
            error: null
        }
    }

    componentDidMount() {
        this.props.getTransactions(Constants.DATA_SOURCE, (response, error) => {
            this.setState({ error: error });
        });
    }

    componentDidUpdate(prevProps) {
        if (this.props.transactionList !== prevProps.transactionList) {
            this.setState({ transactions: this.props.transactionList });
        }
    }

    onItemPressed(item) {
        this.props.navigation.navigate('Detail', { selectedItem: item });
    }

    fetchMoreTransactions = () => {
        // to do: fetch next page of transactions
    }

    handleRefresh = () => {
        this.setState({ refreshing: true, }, () => {
            // to do: handle swipe refresh
        })
    }

    render() {
        const { error, transactions } = this.state;
        // const { transactionCount } = this.props;

        return (
            error != null ?
                <View style={styles.centerContainer}>
                    <Text style={styles.errorText}>{error}</Text>
                </View>
                :
                <View style={styles.container}>
                    <Text style={[styles.headerText, styles.textMargin]}>Transaction history</Text>
                    <FlatList
                        data={transactions}
                        keyExtractor={item => item.id}
                        extraData={this.state}
                        // onEndReached={transactions.length < transactionCount ? this.fetchMoreTransactions : null}
                        // refreshing={this.state.refreshing}
                        // onRefresh={this.handleRefresh}
                        renderItem={
                            ({ item }) => {
                                return (
                                    <TransactionItem
                                        item={item}
                                        onPress={item => this.onItemPressed(item)} />
                                );
                            }
                        }
                    />
                </View>
        )
    }
}

const mapStateToProps = ({ transaction }) => {
    const { transactionList, transactionCount } = transaction;
    return { transactionList, transactionCount };
};

export default connect(
    mapStateToProps, { getTransactions }
)(HomeScreen);