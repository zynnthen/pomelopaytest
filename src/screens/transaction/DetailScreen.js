import React, { Component } from 'react';
import { View, Text, Button, Alert } from 'react-native';
import { connect } from 'react-redux';

import { firstCharUppercase } from "../../utils/string-utils";
import { updateTransaction } from "../../actions"

import COLORS from "../styles/colors";
import styles from "../styles/styles";

class DetailScreen extends Component {
    constructor(props) {
        super(props);
    }

    isRefundable(item) {
        return item.canRefundIfConfirmed && item.refundId === null
    }

    requestRefund(transaction) {
        Alert.alert(
            'Refund',
            'You cannot undo this action.',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('cancelled refund'),
                    style: 'cancel',
                },
                { text: 'OK', onPress: () => this.performRefund(transaction) },
            ],
            { cancelable: false },
        );
    }

    performRefund(transaction) {
        transaction.refundId = Math.floor(Math.random() * 1000);
        this.props.updateTransaction(transaction);
    }

    render() {
        const { selectedItem } = this.props.navigation.state.params;

        return (
            <View style={styles.container}>
                <View style={styles.flexBetweenContainer}>
                    <Text style={[styles.headerText, styles.textMargin]}>
                        {firstCharUppercase(selectedItem.vendor)}
                    </Text>
                    <View style={
                        this.isRefundable(selectedItem)
                            ? customStyles.roundedEnabledView
                            : customStyles.roundedDisabledView
                    } >
                        <Button
                            title="Refund"
                            onPress={() => this.requestRefund(selectedItem)}
                            disabled={!this.isRefundable(selectedItem)}
                            color="white"
                        />
                    </View>
                </View>
                <Text>Details of transaction can be displayed here.</Text>
            </View>
        )
    }
}

const customStyles = {
    roundedEnabledView: {
        backgroundColor: COLORS.DANGER,
        borderRadius: 20,
        margin: 8,
        padding: 2
    },
    roundedDisabledView: {
        backgroundColor: COLORS.DISABLED,
        borderRadius: 20,
        margin: 8,
        padding: 2
    },
}

const mapStateToProps = ({ transaction }) => {
    const { transactionList } = transaction;
    return { transactionList };
};

export default connect(
    mapStateToProps, { updateTransaction }
)(DetailScreen);