
import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { moderateScale } from 'react-native-size-matters';
import { firstCharUppercase } from '../../utils/string-utils';
import { displayDateTime } from '../../utils/time-utils';
import COLORS from '../styles/colors';
import styles from '../styles/styles';

class TransactionItem extends Component {
    static defaultProps = {
        onPress: () => { }
    }

    onRowPress() {
        this.props.onPress(this.props.item);
    }

    render() {
        const { item } = this.props;

        return (
            <TouchableOpacity
                style={[
                    styles.listItemContainer,
                    styles.flexBetweenContainer
                ]}
                onPress={this.onRowPress.bind(this)}
            >
                <View>
                    <Text style={styles.listPrimaryTextStyle}>
                        {firstCharUppercase(item.vendor)}
                    </Text>
                    <Text style={[styles.listSecondaryTextStyle]}>
                        {displayDateTime(item.created)}
                    </Text>
                </View>
                <View>
                    <Text style={[
                        customStyles.transactionAmountText,
                        item.refundId !== null ? customStyles.transactionRefund : null
                    ]}>
                        {`${item.currency} ${item.amount}`}
                    </Text>
                    {
                        item.refundId !== null
                            ? <Text style={customStyles.refundText}>Refunded</Text>
                            : null
                    }
                </View>
            </TouchableOpacity>
        )
    }
}

const customStyles = {
    transactionAmountText: {
        fontSize: moderateScale(16),
        fontWeight: 'bold'
    },
    transactionRefund: {
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid'
    },
    refundText: {
        padding: moderateScale(2),
        fontSize: moderateScale(16),
        color: COLORS.TEXT_ERROR,
        fontWeight: 'bold'
    }
}

export default (TransactionItem);