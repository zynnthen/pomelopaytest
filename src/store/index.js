import thunk from 'redux-thunk';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import reducers from '../reducers';

const rootReducer = combineReducers(reducers);

export default function configurationStore(initialState = {}) {
    const store = createStore(
        rootReducer,
        initialState,
        applyMiddleware(thunk)
    );

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('../reducers', () => {
            const nextRootReducer = require('../reducers/index');
            store.replaceReducer(nextRootReducer);
        });
    }

    return { store };
}
